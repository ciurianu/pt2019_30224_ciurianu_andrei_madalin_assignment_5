

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SplitActivities {
	public List<MonitoredData> listData = new ArrayList<>();
	private Stream<String> stream;
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public void getActivities() {
		try {
			stream = Files.lines(Paths.get("Activities.txt"));

			listData = stream
					.map(line -> line.split("\\t+"))
					.map(array -> new MonitoredData(array[0], array[1], array[2]))
					.collect(Collectors.toCollection(ArrayList::new));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long getDays() {
		String dateStart = listData
				.stream()
				.findFirst()
				.get()
				.getStart_time();
		String dateEnd = listData
				.stream()
				.skip(listData.stream().count() - 1)
				.findFirst().get()
				.getEnd_time();

		LocalDateTime start = LocalDateTime.parse(dateStart, formatter);
		LocalDateTime end = LocalDateTime.parse(dateEnd, formatter);
		return ChronoUnit.DAYS.between(start, end);
	}

	public List<String> getListOfActivities() {
		List<String> activities = new ArrayList<>();
		for (MonitoredData it : listData) {
			if (!activities.contains(it.getActivity_label())) {
				activities.add(it.getActivity_label());
			}
		}
		return activities;
	}

	public Map<String, Integer> mapActivities() {
		Map<String, Integer> result = new HashMap<>();
		List<String> activities = getListOfActivities();
		for (String act : activities) {
			int value = (int) listData
					.stream()
					.filter(d -> d.getActivity_label()
					.equals(act))
					.count();
			String key = act;
			result.put(key, value);
		}
		return result;
	}

	//nu merge
	public List<Map<String, Integer>> mapActivitiesForDay() {
		List<Map<String, Integer>> result = new ArrayList<>();
		Map<String, Integer> map = new HashMap<>();
		String dateStart = listData.stream().findFirst().get().getStart_time();
		LocalDateTime start = LocalDateTime.parse(dateStart, formatter);
		List<String> activities = getListOfActivities();
		String nextDate = listData.stream().skip(1).findFirst().get().getStart_time();
		LocalDateTime next = LocalDateTime.parse(nextDate, formatter);
		int i = 1;
		for (String act : activities) {
			i++;
			if (ChronoUnit.DAYS.between(start, next) < 1) 
			{
				
				nextDate = listData.stream().skip(i).findFirst().get().getStart_time();
				next = LocalDateTime.parse(nextDate, formatter);
				int value = (int) listData
						.stream()
						.skip(map.size())
						.filter(d -> d.getActivity_label().equals(act))
						//.filter(d -> LocalDateTime.parse(d.getStart_time(), formatter), LocalDateTime.parse(d.getStart_time(), formatter).plusDays(1))
						.count();
//						.collect(Collectors.groupingBy(d -> d.getActivity_label(), Collectors.reducing(Duration.ZERO, 
//					                d -> Duration.between(d.get, a.endTime).abs(), Duration::plus);
				String key = act;
				map.put(key, value);
				System.out.println(map);
			} else {
				result.add(map);
				map = new HashMap<>();
				start = next;
				next = start.plusDays(1);
			}

		}
		return result;
	}

	public void durationOfActivityOnLine() {
		//Map<String, Integer> map = new HashMap<>();
		int i = 1;
		for (MonitoredData it : listData) {
			System.out.println("Line " + i++ + ": " + "Duration on seconds for activity " + it.getActivity_label() + " is: " + 
					(int)Duration.between(LocalDateTime.parse(it.getStart_time(), formatter), LocalDateTime.parse(it.getEnd_time(), formatter)).getSeconds());
			//String key = it.getActivity_label();
			//map.put(key, (int)Duration.between(LocalDateTime.parse(it.getStart_time(), formatter), LocalDateTime.parse(it.getEnd_time(), formatter)).getSeconds());
		}
	}

	
	public Map<String, Integer> totalDuration() {
	
		Map<String, Integer> duration = new HashMap<>();
		for(MonitoredData it : listData) {
			if(duration.containsKey(it.getActivity_label())) {
				int x = duration.get(it.getActivity_label());
				x = x + (int)Duration.between(LocalDateTime.parse(it.getStart_time(), formatter), LocalDateTime.parse(it.getEnd_time(), formatter)).getSeconds();
				duration.replace(it.getActivity_label(), duration.get(it.getActivity_label()).intValue(), x);
				//System.out.println("replace");
			}
			else {
				//System.out.println("not replace");
				duration.put(it.getActivity_label(),(int)Duration.between(LocalDateTime.parse(it.getStart_time(), formatter), LocalDateTime.parse(it.getEnd_time(), formatter)).getSeconds());
			}
			
			//System.out.println(it.getActivity_label() +" " +(int)Duration.between(LocalDateTime.parse(it.getStart_time(), formatter), LocalDateTime.parse(it.getEnd_time(), formatter)).getSeconds());
		}
		
		return duration;
	}
	
	
	public static void main(String[] args) {
		SplitActivities s = new SplitActivities();
		s.getActivities();
		System.out.println(s.getDays());
		System.out.println(s.mapActivities());
		System.out.println(s.getListOfActivities());
		s.durationOfActivityOnLine();
		System.out.println(s.mapActivitiesForDay());
		System.out.println(s.totalDuration());
		System.out.println("Snack");
	}
}
